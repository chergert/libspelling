# Chinese (Taiwan) translation for libspelling.
# Copyright (C) 2022 libspelling's COPYRIGHT HOLDER
# This file is distributed under the same license as the libspelling package.
# Freddy <freddy4212@gmail.com>, 2022.
# Freddy Cheng <freddy4212@gmail.com>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: libspelling main\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/libspelling/issues\n"
"POT-Creation-Date: 2024-10-18 12:30-0700\n"
"PO-Revision-Date: 2023-03-07 05:00+0000\n"
"Last-Translator: Weblate Admin <pesder@mail.edu.tw>\n"
"Language-Team: Chinese (Traditional) <http://darkbear.mercusysddns.com/"
"projects/gnome-44/libspelling-main/zh_Hant/>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 4.16.1\n"

#: lib/spelling-menu.c:273
msgid "Add to Dictionary"
msgstr "新增至字典"

#: lib/spelling-menu.c:274
msgid "Ignore"
msgstr "忽略拼寫錯誤"

#: lib/spelling-menu.c:275
msgid "Check Spelling"
msgstr "檢查拼寫"

#: lib/spelling-menu.c:284
msgid "Languages"
msgstr "語言"
